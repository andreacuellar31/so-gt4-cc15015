#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){

    int grado;              
    double x;               
    double *coeficientes;

    printf("Ingrese el grado del polinomio que desea evaluar: ");
    scanf("%d/n",&grado);
    
    for (int i = 0; i < (grado + 1); i++){
        printf("Ingrese el Coeficiente para la posición donde X tenga el valor de:  X%d: ", i);
        scanf("%lf",&coeficientes[i]);
    }
    printf("\n");
    
    printf("Ingrese el valor x para evaluar en el polinomio: ");
    scanf("%lf",&x);
   

    double resultado = 0.0;
    printf("Polinomio : P(X):\nP(%3.2f) = ", x);
    for (int i = 0; i < (grado + 1); i++){
        printf("(%3.2f).(X^%d)", coeficientes[i], i);
        resultado += (coeficientes[i])*(pow(x, i));
        if(i != grado ){
            printf(" + ");
        }
    }
    printf(" = %3.2f\n", resultado);

    return 0;
}

