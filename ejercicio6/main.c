#include <stdio.h>

int valorMaximo(int *list, int tamanio){
    int maximo = list[0];
    for (int i = 0; i < tamanio; i++){
        if(list[i] > maximo){
            maximo = list[i];
        }
    }
    return maximo;
}

int valorMinimo(int *list, int tamanio){
    int minimo = list[0];
    for (int i = 0; i < tamanio; i++){
        if(list[i] < minimo){
            minimo = list[i];
        }
    }
    return minimo;
}

float mediaAritmetica(int *list, int tamanio){
    long total = 0;
    for (int i = 0; i < tamanio; i++){
        total += list[i];
    }
    return (float) total / tamanio;
}


int main(){

     int cantidad;
     printf("Qué cantidad de números desea ingresar? ");
     scanf("%d",&cantidad);
    
     int numeros[cantidad];
     printf("Ingrese cada número\n");
     for (int i = 0; i < cantidad; i++){
     scanf("%d/n",&numeros[i]);
     }

    printf("El número menor es: %d\r\n", valorMinimo(numeros, cantidad));
    printf("El número mayor es: %d\r\n", valorMaximo(numeros, cantidad));
    printf("El promedio de los números ingresados es: %3.2f\r\n", mediaAritmetica(numeros, cantidad));

    return 0;
}


