#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *pedirTexto();
void contarVocales(char *entrada, int *numeros);
void imprimir(int *num);

void main(){
    char *texto;
    int num[5];
    texto = pedirTexto();
    contarVocales(texto, num);
    imprimir(num);
}

char *pedirTexto(){
    char * t;
    t = (char *) malloc(32);
    printf("Ingrese el texto: ");
    scanf("%s",t);
    return t;
}
void contarVocales(char *entrada, int *numeros){
    char *vocales[] = {"aA", "eE", "iI", "oO", "uU"};
    for (int i = 0; i < 5; i++){
        numeros[i] = 0;
        char *c = entrada;
        while (*c){
            if (strchr(vocales[i], *c)){
                numeros[i] += 1;
            }
            c++;
        }
    }

}
void imprimir(int *numeros){
    printf("Letras en el texto\naA: %d\neE: %d\niI: %d\noO: %d\nuU: %d\n", numeros[0], numeros[1], numeros[2], numeros[3], numeros[4]);
}

